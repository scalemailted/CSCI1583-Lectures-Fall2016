
public class SalaryEmployee extends Employee
{
    private double monthlySalary;
    
    public SalaryEmployee(String firstName, String lastName, Date dob, Date hireDate, double salary)
    {
        super(firstName, lastName, dob, hireDate);
        if (salary <= 0)
        {
            throw new IllegalArgumentException("Salary must be greater than $0");
        }
        this.monthlySalary = salary;
    }
    
    public double getMonthlySalary()
    {
        return this.monthlySalary;
    }
    
    public double getEarnings()
    {
        return this.monthlySalary/2;
    }
    
    public String toString()
    {
        String employee = super.toString();
        employee = employee.replace("Employee", "Salary Employee");
        return String.format("%sSalary: $%.2f", employee, this.monthlySalary);
    }
}