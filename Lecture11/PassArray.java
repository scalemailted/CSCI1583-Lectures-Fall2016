public class PassArray
{
    public static void main(String[] args)
    {
        int[] array = {1,2,3,4,5};
        
        System.out.println(array[3]);
        modifyElement(array[3]);
        System.out.println(array[3]);
        
        displayArray(array);
        modifyArray(array);
        displayArray(array);
        
    }
    
    public static void displayArray(int[] arr)
    {
        for (int x : arr)
        {
            System.out.printf("%d\n",x);
        }
    }
    
    public static void modifyArray(int[] arr)
    {
        for (int i=0; i<arr.length; i++)
        {
            arr[i] *= 5; 
        }
    }
    
    public static void modifyElement(int element)
    {
        element *= 5;
        System.out.println(element);
    }
}