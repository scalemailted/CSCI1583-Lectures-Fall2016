public class CommandLineArgs
{
    public static void main(String[] args)
    {
        if (args.length < 2)
        {
            System.out.println("You must pass in 2 arguments");
        }
        else
        {
            System.out.println(args[0]);
            System.out.println(args[1]);
        }
    }
}