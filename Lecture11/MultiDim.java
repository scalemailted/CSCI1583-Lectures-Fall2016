public class MultiDim
{
    public static void main(String[] args)
    {
        String[][] name = { 
                            {"Ted", "Alex", "Joe"}, 
                            {"Mary", "Lily", "Jo", "Tim" },
                            {"Bill", "Ed"}
                          };
                          
        for (int i=0; i<name.length; i++)
        {
            for (int j=0; j<name[i].length; j++)
            {
                String n = name[i][j];
                System.out.printf("%s ",n);
            }
            System.out.println();
        }
    }
}