public abstract class Employee implements Payable
{
    //instance variables (attributes)
    private String firstName;
    private String lastName;
    private Date dob;
    private Date hireDate;
    
    //Constructor
    public Employee(String firstName, String lastName, Date dob, Date hireDate)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.dob = dob;
        this.hireDate = hireDate;
    }
    
    //getters
    public String getFirstName()
    {
        return this.firstName;
    }
    
    public String getLastName()
    {
        return this.lastName;
    }
    
    public Date getDOB()
    {
        return this.dob;
    }
    
    public Date getHireDate()
    {
        return this.hireDate;
    }
    
    public String toString()
    {
        return String.format("Employee:\n%s, %s\nDOB:  %s\nHire: %s\n",
                              this.getLastName(),
                              this.getFirstName(),
                              this.getDOB(),
                              this.getHireDate());
    }
    
    public abstract double getEarnings();
    
    public double getCost()
    {
        return this.getEarnings();
    }
}