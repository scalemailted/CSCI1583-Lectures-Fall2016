public class Invoice implements Payable
{
    //attributes
    private double totalCost;
    private String name;
    private String id;
    
    //constructor
    public Invoice(double cost, String name, String id)
    {
        this.totalCost = cost;
        this.name = name;
        this.id = id;
    }
    
    //get cost
    public double getCost()
    {
        return this.totalCost;
    }
    
    //toString
    public String toString()
    {
        return String.format("Invoice# %s\n%s: $%.02f",
                             this.id,
                             this.name,
                             this.totalCost);
    }
}