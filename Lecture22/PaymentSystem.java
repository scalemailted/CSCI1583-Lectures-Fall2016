public class PaymentSystem
{
    public static void main(String[] args)
    {
        double sum = 0;
        
        Invoice nov = new Invoice(100.00, "Santa Claus Co.", "Nov2016-001");
        Invoice dec = new Invoice(500000.00, "Elftown Inc", "Dec2016-001");
        Employee kim = new SalaryEmployee("Kim","Green",new Date(5,7,1940), new Date(1,1,2012),5000);
        Employee jim = new CommissionEmployee("Jim","Thumb",new Date(30,8,2014), new Date(31,10,2016),5000.25, 0.25);
        Employee jack = new CommissionEmployee("Jack","Jackson",new Date(14,8,2016), new Date(3,11,2016),10, 0.5);
        Employee tom = new SalaryEmployee("Tom","Blue",new Date(7,3,1970), new Date(1,1,2017),5000);
        
        
        
        Payable[] costlyThings = {kim, jim, jack, tom, nov, dec};

        for (Payable i : costlyThings)
        {
            sum += i.getCost();
        }
        
        System.out.printf("The total costs: %.02f\n",sum);
    }
}