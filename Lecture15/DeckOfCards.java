import java.util.Random;

public class DeckOfCards
{
    public final static int NUMBER_OF_CARDS = 52;
    
    private Card[] deck;
    private int topCard;
    private Random randomizer;
    
    public DeckOfCards()
    {
        this.randomizer = new Random();
        this.topCard = 0;
        this.deck = new Card[NUMBER_OF_CARDS];
        
        String[] suits = {"Diamonds", "Spades", "Hearts", "Clubs"};
        String[] ranks = {"Ace", "Deuce", "Three", "Four", "Five",
                          "Six", "Seven", "Eight", "Nine", "Ten",
                          "Jack", "Queen", "King"};
                          
        for (String suit : suits)
        {
            for (String rank : ranks)
            {
                this.deck[this.topCard++] = new Card(suit, rank); 
            }
        }
        
        this.topCard = 0;
        this.shuffle();
    }
    
    public Card deal()
    {
        if (this.topCard >= NUMBER_OF_CARDS)
        {
            this.topCard = 0;
        }
        return this.deck[topCard++];
    }
    
    public void shuffle()
    {
        for (int currentCard=0; currentCard<NUMBER_OF_CARDS; currentCard++)
        {
            int randomCard = randomizer.nextInt(NUMBER_OF_CARDS);
            Card thisCard = this.deck[currentCard];
            this.deck[currentCard] = this.deck[randomCard];
            this.deck[randomCard] = thisCard;
        }
    }
    
    public String toString()
    {
        String cardString = "";
        for (Card card: this.deck)
        {
            cardString += String.format("%s\n",card);
        }
        return cardString;
    }
}