/*
 * Guess the number and win the game
 */
 import java.util.Scanner;
 import java.util.Random;
 
 public class Guess2
 {
    public static void main(String[] args)
    {
        Random numberGenerator = new Random();
        int number = numberGenerator.nextInt(1000)+1;
        
        int guesses = 10;
        boolean win = false;
        
        int guess = 0;
        while (guesses > 0 && !win)
        {
            System.out.printf("You have %d left.\n", guesses);
            System.out.println("Guess a number between 1 - 1000");
            
            Scanner input = new Scanner(System.in);
            guess = input.nextInt();
            
            if (guess == number)
            {
                win = true;
            }
            else if (guess < number)
            {
                System.out.println("Too Low");
            }
            else if (guess > number)
            {
                System.out.println("Too High");
            }
            guesses--;
        }//end while
        
        if (guess == number)
        {
            System.out.printf("You Won! You guessed %d in %d guesses.\n", number, 10-guesses); 
        }
        else
        {
            System.out.printf("You Lost! The number was: %d\n", number); 
        }
    }//end main
 }//end class
 