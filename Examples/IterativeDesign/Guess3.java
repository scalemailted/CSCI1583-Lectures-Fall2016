/*
 * Guess the number and win the game
 */
 import java.util.Scanner;
 
 public class Guess3
 {
    public static void main(String[] args)
    {
        System.out.println("Think of a number between 1-1000.");
        
        int tries = 10;
        boolean win = false;
        
        int max = 1000;
        int min = 1;
        while (tries > 0 && !win)
        {
            int guess = min+(max-min)/2;
            
            System.out.printf("I have %d tries left.\n", tries);
            System.out.printf("Is your number %d?\n", guess);
            System.out.println("[Y]es, [H]igh, [L]ow");
            
            Scanner input = new Scanner(System.in);
            String response = input.next();
            
            if (response.equals("Y"))
            {
                win = true;
            }
            else if (response.equals("H"))
            {
                max -= (max-min+1)/2;
            }
            else if (response.equals("L"))
            {
                min += (max-min+1)/2;
            }
            tries--;
        }//end while
        System.out.printf("I Won! I guessed it in %d guesses.\n", 10-tries);
        
    }//end main
 }//end class
 