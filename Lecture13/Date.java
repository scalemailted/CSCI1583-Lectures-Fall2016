public class Date
{
    //Attributes for a Date
    private int year;
    private int month;
    private int day;
    
    //constructor
    public Date(int day, int month, int year)
    {
        this.day=day;
        this.month=month;
        this.year=year;
    }
    
    //getters
    public int getDay()
    {
        return this.day;
    }
    
    public int getMonth()
    {
        return this.month;
    }
    
    public int getYear()
    {
        return this.year;
    }
    
    public String toString()
    {
        return String.format("%d/%d/%d",this.month, this.day, this.year);
    }

}