import java.util.Random;

public class Dice
{
    public static int rollDice(int... dice)
    {
        Random randomGenerator = new Random();
        int diceSum = 0;
        for (int die : dice)
        {
            diceSum += randomGenerator.nextInt(die)+1;
        }
        return diceSum;
    }
    
}