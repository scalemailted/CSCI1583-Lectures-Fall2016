import java.util.Scanner;

public class AccountTester
{
    public static void main(String[] args)
    {
        //create a scanner
        Scanner input = new Scanner(System.in);
        
        //create an account
        Account account1 = new Account("Holmberg", "Ted");
        Account account2 = new Account("Wilco", "Ronny");
        
        //display the initial state of account variables
        System.out.println(account1);
        System.out.println(account2);
        
        //prompt for a name
        System.out.print("Enter first & last name:");
        String first = input.next();
        String last = input.next();
        
        
        //Set name into the account
        account1.setFirstName(first);
        account1.setLastName(last);
        
        //prompt for a name
        System.out.print("Enter first & last name:");
        first = input.next();
        last = input.next();
        
        
        //Set name into the account
        account2.setFirstName(first);
        account2.setLastName(last);
        
        //display the new state of account
        System.out.println(account1.getFirstName());
        System.out.println(account1.toString());
        System.out.println(account2.toString());
    }
}