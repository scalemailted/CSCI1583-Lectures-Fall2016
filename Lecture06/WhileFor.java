public class WhileFor
{
    public static void main(String[] args)
    {
        int i = 0;
        while (i < 10)
        {
            System.out.printf("%d ",i);
            i++;
        }
        System.out.println();
        
        for (int j=0; j<10; j++)
        {
            System.out.printf("%d ", j);
        }
        System.out.println();
        
        int x = 0;
        for (; x<10;)
        {
            System.out.printf("%d ", x);
            x++;
        }
        System.out.println();
    }
}