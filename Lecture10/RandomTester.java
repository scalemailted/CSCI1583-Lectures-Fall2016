
import java.util.Random;

public class RandomTester
{
	public static void main(String[] args)
	{
		//the count for each face
		int[] frequency = new int[13];
		for (int i=0; i<1000000; i++)
		{
			int diceSum = rollDice();
			frequency[diceSum]=frequency[diceSum]+1;
		}

		for(int i=2;i<frequency.length;i++)
		{
			System.out.printf("%d: %d\n",i,frequency[i]);
		}

	}

	public static int rollDice()
	{
		Random randomGenerator = new Random();
		int die1 = randomGenerator.nextInt(6)+1;
		int die2 = randomGenerator.nextInt(6)+1;
		return die1 + die2;
	}
}