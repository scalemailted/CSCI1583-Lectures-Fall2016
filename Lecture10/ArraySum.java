public class ArraySum
{
	public static void main(String[] args)
	{
		int[] arr = new int[10];
		int[] array = {23,43,34,56,222};
		int sum = 0;
		for (int x : array)
		{
			sum += x;
		}
		System.out.printf("The sum is %d\n", sum);


		printArray(array);

		int[] y = array;
		printArray(y);

		for(int i=0;i<array.length;i++)
		{
			array[i]*=10;
		}

		printArray(y);

	}

	public static void printArray(int[] arr){
		for (int element : arr)
		{
			System.out.println(element);
		}

	}
}