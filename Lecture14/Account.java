public class Account
{
	private static int numberOfAccounts = 0;

	//attributes that define account
	private String firstName;
	private String lastName;
	private int accountID;
	private double balance;

	//Define how to construct an account
	public Account(String first, String last, double balance)
	{
		this.firstName = first;
		this.lastName = last;
		if (balance >= 0.0)
		{
			this.balance = balance;
		}
		this.accountID=numberOfAccounts++;
	}

	//withdraw
	public void withdraw(double amount)
	{
		if (amount > 0.0 && this.balance >= amount)
		{
			this.balance -= amount;
		}
		else
		{
			System.out.println("You don't have enough money!");
		}
	}

	//deposit
	public void deposit(double amount)
	{
		if (amount > 0.0)
		{
			this.balance += amount;
		}
	}

	//Stringify the account object as text
	public String toString()
	{
		return String.format("id:%03d %s, %s: $%.2f", this.accountID ,this.lastName, this.firstName, this.balance);
	}

	public static int getNumberOfAccounts()
	{
		return Account.numberOfAccounts;
	}




}