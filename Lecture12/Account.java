public class Account
{
    //Attributes of an Account
    private String lastName;
    private String firstName;
    
    //constructor
    public Account()
    {
        
    }
    
    //getters
    public String getLastName()
    {
        return this.lastName;
    }
    
    public String getFirstName()
    {
        return this.firstName;
    }
    
    //setters
    public void setLastName(String lastName)
    {
        this.lastName = lastName;
    }
    
    public void setFirstName(String firstName)
    {
        this.firstName = firstName;
    }
    
    //toString method
    public String toString()
    {
        return String.format("First: %s Last: %s",this.firstName, this.lastName);
    }
    
    
}