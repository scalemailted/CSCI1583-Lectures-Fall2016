import java.util.ArrayList;

public class ArrayListTest
{
    public static void main(String[] args)
    {
        ArrayList<String> items = new ArrayList<String>();
        items.add("red");
        display(items);
        items.add(0,"yellow");
        display(items);
        
        //print array
        System.out.println(items.toString());
        
        //get an items from arraylist
        System.out.println(items.get(0));
        
        //remove an item
        items.remove("yellow");
        System.out.println(items);
        
        //Check if list has an item
        boolean b = items.contains("yellow");
        System.out.printf("That arraylist contains yellow? %b\n",b);
        
        //Check the size of an arraylist
        System.out.printf("The size is %d\n", items.size());
        
        
        
    }
    
    public static void display(ArrayList<String> items)
    {
        for (String s : items)
        {
            System.out.println(s);
        }
        System.out.println();
    }
}