import java.util.Arrays;

public class ArrayClassMethods
{
    public static void main(String[] args)
    {
        int[] array = {2,4,3,5,1};
        
        //Sort array
        Arrays.sort(array);
        System.out.println("Sorted array:");
        printArray(array);
        
        //Fill array
        int[] fillArray = new int[10];
        Arrays.fill(fillArray, 7);
        System.out.println("Fill array:");
        printArray(fillArray);
        
        //Copy array
        int[] arrayCopy = new int[array.length];
        System.arraycopy(array, 0, arrayCopy, 0, array.length);
        System.out.println("Copy array:");
        printArray(arrayCopy);
        
        //Equality
        boolean b = Arrays.equals(array, arrayCopy);
        System.out.println(b);
        
        //search
        int location = Arrays.binarySearch(array, 3);
        System.out.printf("The location of 3 is the index of: %d\n", location);
        
    }
    
    public static void printArray(int[] arr)
    {
        for (int i: arr)
        {
            System.out.println(i);
        }
    }
}