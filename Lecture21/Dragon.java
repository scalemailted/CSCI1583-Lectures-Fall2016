public class Dragon extends Monster
{
    public Dragon(String name)
    {
        super(name);
    }
    
    public void attack(GameCharacter enemy)
    {
        System.out.println("The dragon attacks");
    }
    
    public void breathFire()
    {
        System.out.println("You die from fire!");
    }
    
    public String toString()
    {
        return this.getName() + " the Dragon";
    }
}