public class Hero extends GameCharacter
{
    public Hero(String name)
    {
        super(name);
    }
    
    public void attack(GameCharacter enemy)
    {
        System.out.println("The hero attacks");
    }
    
    public String toString()
    {
        return super.toString() + "the Hero";
    }
}