public class GameTester
{
    public static void main(String[] args)
    {
        GameCharacter gc = new Dragon("Tom");
        Monster m = new Dragon("Tim");
        GameCharacter h = new Hero("Lisa");
        System.out.println(gc);
        System.out.println(m);
        
        gc.attack(m);
        m.getTreasure();
        
        if (m instanceof Dragon)
        {
            Dragon d = (Dragon) m;
            d.breathFire();
        }
        
    }
}