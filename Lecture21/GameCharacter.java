public abstract class GameCharacter
{
    private String name;
    
    public GameCharacter(String name)
    {
        this.name = name;
    }
    
    public String getName()
    {
        return this.name;
    }
    
    public abstract void attack(GameCharacter enemy);
    
    public String toString()
    {
        return this.name;
    }
}