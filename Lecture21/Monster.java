public abstract class Monster extends GameCharacter
{
    
    public Monster(String name)
    {
        super(name);
    }
    
    public void getTreasure()
    {
        System.out.println("You fonud treasure!");
    }
    
    public String toString()
    {
        return super.toString() + " the Monster";
    }
}