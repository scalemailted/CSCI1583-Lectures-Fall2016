public class Test
{
    public static void main(String[] args)
    {

        Employee tom = new SalaryEmployee("Tom","Blue",new Date(7,3,1970), new Date(1,1,2017),5000);
        if (tom instanceof SalaryEmployee)
        {
            SalaryEmployee e = (SalaryEmployee) tom;
            System.out.println(e.getMonthlySalary());
        }
    }
}