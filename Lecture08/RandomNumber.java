import java.util.Random;

public class RandomNumber
{
    public static void main(String[] args)
    {
        int count1=0;
        int count2=0;
        int count3=0;
        int count4=0;
        int count5=0;
        int count6=0;
        
        Random randomGenerator = new Random();
        randomGenerator.setSeed(1);
        for (int i=0; i< 6000000; i++)
        {
            int number = randomGenerator.nextInt(6)+1;
            switch(number)
            {
                case 1: 
                    {
                        count1++;
                        break;
                    }
                case 2: 
                    {
                        count2++;
                        break;
                    }
                case 3: 
                    {
                        count3++;
                        break;
                    }
                case 4: 
                    {
                        count4++;
                        break;
                    }
                case 5: 
                    {
                        count5++;
                        break;
                    }
                case 6: 
                    {
                        count6++;
                        break;
                    }
            }
        }
        System.out.printf("1: %d\n", count1);
        System.out.printf("2: %d\n", count2);
        System.out.printf("3: %d\n", count3);
        System.out.printf("4: %d\n", count4);
        System.out.printf("5: %d\n", count5);
        System.out.printf("6: %d\n", count6);
    }
}