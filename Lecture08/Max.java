public class Max
{
    public static void main(String[] args)
    {
        int result = maxValue(34,5,28);
        System.out.printf("the max value is %d", result);
        double result2 = maxValue(1.2,3.4,-2.4);
        System.out.printf("the max value is %f", result2);
        int result3 = maxValue(34,54);
        System.out.printf("the max value is %d", result3);
    }
    
    public static int maxValue(int x, int y, int z)
    {
        int max = x;
        if (y > max)
        {
            max = y;
        }
        if (z > max)
        {
            max = z;
        }
        
        return max;
    }
    
    public static double maxValue(double x, double y, double z)
    {
        double max = x;
        if (y > max)
        {
            max = y;
        }
        if (z > max)
        {
            max = z;
        }
        
        return max;
    }
    
        public static int maxValue(int x, int y)
    {
        int max = x;
        if (y > max)
        {
            max = y;
        }
        
        return max;
    }
}