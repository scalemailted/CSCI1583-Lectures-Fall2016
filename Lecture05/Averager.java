//Create a program that averages 10 grades

import java.util.Scanner;

public class Averager
{
    public static void main(String[] args)
    {
        //setup scanner for user input
        Scanner kb = new Scanner(System.in);
        
        //variable to hold a sum
        int sum = 0;
        //variable to hold the count
        int count = 0;
        //variable to hold the average
        double average;
        
        //while the current count is less than 10
        while (count < 10)
        {
            //prompt the user to enter a new grade
            System.out.print("Enter a grade: ");
            //create variable to hold grade and get the users input for that grade
            int grade = kb.nextInt();
            //increment our count variable
            count += 1;
            //add the grade variable to our sum
            sum += grade;
        }
            
        //calculate the average
        average = (double)(sum)/count;
        //Report the result to the user
        System.out.printf("The average is: %f\n",average);
    }
}


