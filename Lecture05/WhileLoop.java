//find the product of 3 times itself until the result is more than 100  
public class WhileLoop
{
    public static void main(String[] args)
    {
        //create a variable and set it to 3
        int product = 3;
        
        //while product of the variable is 
        //less than 100 we repeat
        while (product > 100)
        {
            product = product * 3;
            System.out.printf("Product is: %d\n",product);
        }
        
    }
}