public class CommissionEmployee extends Employee
{
    //instance variables (attributes/properties)
    private double grossSale;
    private double commissionRate;
    
    public CommissionEmployee(String first, String last, Date dob, Date hire, double sales, double rate)
    {
        super(first, last, dob, hire);
        this.grossSale = sales;
        this.commissionRate = rate;
    }
    
    public double earnings(){
        return this.grossSale * this.commissionRate;
    }
    
    public String toString()
    {
        String employee = super.toString();
        employee = employee.replace("Employee", "Commission Employee");
        return String.format("%sEarning: $%.2f", employee, this.earnings());
    }
}