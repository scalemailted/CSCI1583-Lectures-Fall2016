public class ObjectTester
{
    public static void main(String[] args)
    {
        Object[] array= {1, "Hello", new Date(25,12,1999), true};
        for (Object item : array)
        {
            System.out.println(item.getClass());
        }
        
    }
}