public class EmployeeTester
{
    public static void main(String[] args)
    {
        Employee tim = new Employee("Tim","Fletcher",new Date(25,12,1999), new Date(1,1,2012));
        System.out.println(tim);
        
        SalaryEmployee kim = new SalaryEmployee("Kim","Green",new Date(5,7,1940), new Date(1,1,2012),5000);
        System.out.println(kim);
        System.out.println();
        
        CommissionEmployee jim = new CommissionEmployee("Jim","Thumb",new Date(30,8,2014), new Date(31,10,2016),5000, 0.25);
        System.out.println(jim);
        
        
    }
}