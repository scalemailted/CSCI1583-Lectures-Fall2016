public class DateTester
{
    public static void main(String[] args)
    {
        Date d1 = new Date(31,10,1999);
        System.out.println(d1);
        Date d2 = new Date(28,2,1999);
        System.out.println(d2);
        Date d3 = new Date(29,2,2000);
        System.out.println(d3);
        Date d4 = new Date(32,10,1999);
        System.out.println(d4);
    }
}