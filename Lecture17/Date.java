public class Date
{
    //class variable for valid days
    private static final int[] DAYS_PER_MONTH =
    {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
    
    //Attributes for a Date
    private int year;
    private int month;
    private int day;
    
    //constructor
    public Date(int day, int month, int year)
    {
        this.setDate(year, month, day);
    }
    
    private void setDate(int year, int month, int day)
    {
        this.setYear(year);
        this.setMonth(month);
        this.setDay(day);
    }
    
    private void setYear(int year)
    {
        if (year < 1900)
        {
            throw new IllegalArgumentException("The year must be after 1900");
        }
        this.year=year;
    }
    
    private void setMonth(int month)
    {
        if (month <= 0 || month > 12)
        {
            throw new IllegalArgumentException("The month must be between 1-12");
        }
        this.month = month;
    }
    
    private void setDay(int day)
    {
        int month = this.getMonth();
        int year = this.getYear();
        if (day <= 0 || day > DAYS_PER_MONTH[month] && (month != 2 && day != 29) )
        {
            throw new IllegalArgumentException("invalid day for month");
        }
        if ( (day==29 && month==2) && !((year % 4==0 && year % 100 != 0) || year % 400 == 0) )
        {
            throw new IllegalArgumentException("invalid day for month");
        }
        this.day = day;
    }
        
    
    //getters
    public int getDay()
    {
        return this.day;
    }
    
    public int getMonth()
    {
        return this.month;
    }
    
    public int getYear()
    {
        return this.year;
    }
    
    public String toString()
    {
        return String.format("%d/%d/%d",this.month, this.day, this.year);
    }

}