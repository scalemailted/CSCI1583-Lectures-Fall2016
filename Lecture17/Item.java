public enum Item
{
    KEY("This is a silver key", 0, 0),
    SWORD("This is a sharp sword",10,0),
    SHIELD("Ths is a wooden shield",0,10);
    
    private final String description;
    private final int attack;
    private final int defense;
    private int quantity;
    
    Item(String description, int attack, int defense)
    {
        this.description = description;
        this.attack = attack;
        this.defense = defense;
    }
    
    public String getDescription()
    {
        return this.description;
    }
    
    public int getAttack()
    {
        return this.attack;
    }
    
    public int getDefense()
    {
        return this.defense;
    }
    
    public void incrementQuantity()
    {
        this.quantity++;
    }
    
    public void decrementQuantity()
    {
        this.quantity--;
    }
    
    public int getQuantity()
    {
        return this.quantity;
    }
}
