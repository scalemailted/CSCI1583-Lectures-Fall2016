public class Time
{
    //Attibutes or Instance Variables
    private int hours;
    private int minutes;
    private int seconds;
    
    public Time(int hours, int minutes, int seconds)
    {
        this.setTime(hours, minutes, seconds);
    }
    
    public Time(int hours, int minutes)
    {
        this(hours, minutes, 0);
    }
    
    public Time(int hours)
    {
        this(hours, 0, 0);
    }
    
    public Time()
    {
        this(0, 0, 0);
    }
    
    public void setTime(int hours, int minutes, int seconds)
    {
        this.setHours(hours);
        this.setMinutes(minutes);
        this.setSeconds(seconds);
    }
    
    private void setHours(int hours){
        if (hours < 0 || hours >= 24)
        {
            throw new IllegalArgumentException("hour must be 0-23");
        }
        this.hours = hours;
    }
    
    private void setMinutes(int minutes){
        if (minutes < 0 || minutes >= 60)
        {
            throw new IllegalArgumentException("minutes must be 0-60");
        }
        this.minutes = minutes;
    }
    
    private void setSeconds(int seconds){
        if (seconds < 0 || seconds >= 60)
        {
            throw new IllegalArgumentException("seconds must be 0-60");
        }
        this.seconds = seconds;
    }
    
    public String getUniversal()
    {
        return String.format("%02d:%02d:%02d", this.hours, this.minutes, this.seconds);
    }
    
    public String toString()
    {
        return String.format("%d:%02d:%02d %s",
                            (this.hours==0 || this.hours==12)?12:this.hours%12,
                            this.minutes,
                            this.seconds,
                            (this.hours < 12) ? "AM" : "PM");
    }
}