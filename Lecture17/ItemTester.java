public class ItemTester
{
    public static void main(String[] args)
    {
        Item item = Item.SWORD;
        System.out.println(item.getDescription());
        System.out.printf("Attack: %d\n",item.getAttack());
        System.out.printf("Defense: %d\n",item.getDefense());
        System.out.printf("Quantity: %d\n",item.getQuantity());
        item.incrementQuantity();
        System.out.printf("Quantity: %d\n",item.getQuantity());
    }
}