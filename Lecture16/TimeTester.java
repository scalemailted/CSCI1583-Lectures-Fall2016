public class TimeTester
{
    public static void main(String[] args)
    {
        Time t1;
        try
        {
            t1 = new Time(45,0,0);
        }
        catch (IllegalArgumentException e)
        {
            t1 = new Time(20,20,20);
        }
        Time t2 = new Time(1,1);
        Time t3 = new Time(13);
        Time t4 = new Time();
        System.out.println(t1);
        System.out.println(t2);
        System.out.println(t3);
        System.out.println(t4);
        System.out.println(t1.getUniversal());
        System.out.println(t2.getUniversal());
        //t1.setTime(0,0,0);
        //System.out.println(t1);
        //System.out.println(t1.getUniversal());
    }
}