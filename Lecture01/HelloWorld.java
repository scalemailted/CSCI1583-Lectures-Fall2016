//Output operation
public class HelloWorld
{
    //Three different print methods
    public static void main(String[] args)
    {
        System.out.print("Hello World\n");
        System.out.println("Hello World");
        System.out.printf("%s\n","Hello World");
        
    }
}