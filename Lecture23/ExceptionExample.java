import java.util.Scanner;
import java.util.InputMismatchException;

public class ExceptionExample
{
    static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        boolean continueLoop = true;
        do{
            try 
            {
                //prompt user for number 1
                System.out.print("Enter numerator: ");
                //get number 1 and save it
                int numerator = input.nextInt();
                
                //prompt user for number 2
                System.out.print("Enter denominator: ");
                //get number 2 and save it
                int denominator = input.nextInt();
            

                //invoke the division method
                int result = divide(numerator, denominator);
                //print result
                System.out.printf("The result is: %d\n",result);
                continueLoop = false;
            }
            catch (ArithmeticException e)
            {
                System.out.printf("Exception: You cannot divide by 0\n");
            }
            catch (InputMismatchException e)
            {
                System.out.printf("Exception: input must be an integer\n");
                input.nextLine();
            }
        }while(continueLoop);
        
    }
    
    public static int divide(int num, int den) throws ArithmeticException
    {
        return num/den;
    }
    
    
}