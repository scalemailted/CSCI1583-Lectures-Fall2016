import java.util.Random;

public class Craps
{
    /*What data do we need?*/
    private enum Status {CONTINUE, LOST, WON};
    
    private static final int SNAKE_EYES = 2;
    private static final int TREY = 3;
    private static final int SEVEN = 7;
    private static final int YO = 11;
    private static final int BOXCAR = 12;
    
    private static Status gameStatus;
    private static int myPoint;
    
    public static void main(String[] args)
    {
        firstRound();
        while (gameStatus == Status.CONTINUE)
        {
            nextRound();
        }
        printResult();
    }
    
    //First Round
    public static void firstRound()
    {
        //roll dice
        int diceSum = rollDice();
        //did we win? (7,11)
        if (diceSum == SEVEN || diceSum == YO)
        {
            //we win
            gameStatus = Status.WON;
        }
        //did we lose? (2,3,12)
        else if (diceSum == SNAKE_EYES
                || diceSum == TREY
                || diceSum == BOXCAR)
        {
            //we lose
            gameStatus = Status.LOST;
        }
        //anything else goes to NextRound
        else
        {
            System.out.printf("Your point is %d\n",diceSum);
            myPoint = diceSum;
            gameStatus = Status.CONTINUE;
        }
    }

    
    //Next Round
    public static void nextRound()
    {
        //roll die
        int diceSum = rollDice();
        //did we win? (the same value from round1)
        if (diceSum == myPoint)
        {
            gameStatus = Status.WON;
        }
        //did we lose? (7)
        else if (diceSum == SEVEN)
        {
            gameStatus = Status.LOST;
        }
    }
    
    //Report Result
    
    public static int rollDice()
    {
        Random randomGenerator = new Random();
        int dice1 = randomGenerator.nextInt(6)+1;
        int dice2 = randomGenerator.nextInt(6)+1;
        int diceSum = dice1 + dice2;
        System.out.printf("%d+%d=%d\n",dice1,dice2,diceSum);
        return diceSum;
    }
    
    public static void printResult()
    {
        if (gameStatus == Status.WON)
        {
            System.out.println("You won!");
        }
        else
        {
            System.out.println("You lost!");
        }
    }

}
